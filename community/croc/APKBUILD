# Contributor: André Klitzing <aklitzing@@gmail.com>
# Maintainer: André Klitzing <aklitzing@gmail.com>
pkgname=croc
pkgver=9.6.2
pkgrel=0
pkgdesc="Easily and securely send things from one computer to another"
url="https://github.com/schollz/croc"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/schollz/croc/archive/refs/tags/v$pkgver/croc-$pkgver.tar.gz"
options="net" # fetch dependencies

# secfixes:
#   9.1.0-r0:
#     - CVE-2021-31603

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export CGO_LDFLAGS="$LDFLAGS"
	export GOFLAGS="$GOFLAGS -trimpath -mod=vendor"
	go mod vendor
	go build
}

check() {
	go test ./...
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin croc
}

sha512sums="
a3ad2f6a4bc6a45fa356963bd123ea755caa30c6a3b63e63cc67823ce27d44d62610ff9bfa498f694e356f673c3fcb3cec7ac669bedcde6eaf63111a78538965  croc-9.6.2.tar.gz
"
