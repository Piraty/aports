# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=esbuild
pkgver=0.17.8
pkgrel=0
pkgdesc="Extremely fast JavaScript bundler and minifier"
url="https://esbuild.github.io/"
license="MIT"
arch="all"
makedepends="go nodejs"
source="https://github.com/evanw/esbuild/archive/v$pkgver/esbuild-$pkgver.tar.gz"
options="net" # fetch dependencies

export GOPATH="$srcdir"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags="-X main.version=$pkgver" \
		-v ./cmd/esbuild

	node scripts/esbuild.js npm/esbuild/package.json --version
	node scripts/esbuild.js ./esbuild --neutral

	# binary path override
	sed -i '1s#^#var ESBUILD_BINARY_PATH = "/usr/bin/esbuild";\n#' \
		npm/esbuild/lib/main.js
}

check() {
	go test ./...
}

package() {
	install -Dm755 esbuild "$pkgdir"/usr/bin/esbuild

	local destdir=/usr/lib/node_modules/esbuild

	install -d \
		"$pkgdir"/$destdir/bin \
		"$pkgdir"/$destdir/lib

	install -Dm644 -t "$pkgdir"/$destdir npm/esbuild/package.json
	install -Dm644 -t "$pkgdir"/$destdir/lib npm/esbuild/lib/*
	ln -s /usr/bin/esbuild "$pkgdir"/$destdir/bin/esbuild
}

sha512sums="
4c937006b4afb2f0adbe06069fd3c70015cb1aa131e9344bd0be0276667a20e246da9ac28de91586498957d2ca8f6e9f01abfa2230ebef526edb64c71caafe6c  esbuild-0.17.8.tar.gz
"
